package com.example.uapv1904079.tp2_nicolet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        final BookDbHelper bookDb = new BookDbHelper(this);

        final Book book = (Book) getIntent().getParcelableExtra("book");
        final long id = (long) getIntent().getLongExtra("bookId", 0);

        final TextView nameBook = (TextView) findViewById (R.id.nameBook);
        nameBook.setText(book.getTitle());

        final TextView autheursBook = (TextView) findViewById (R.id.editAuthors);
        autheursBook.setText(book.getAuthors());

        final TextView yearBook = (TextView) findViewById (R.id.editYear);
        yearBook.setText(book.getYear());

        final TextView genresBook = (TextView) findViewById (R.id.editGenres);
        genresBook.setText(book.getGenres());

        final TextView publisherBook = (TextView) findViewById (R.id.editPublisher);
        publisherBook.setText(book.getPublisher());


        Button saveButton = (Button) findViewById(R.id.button);
        assert saveButton != null;

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                book.setTitle(nameBook.getText().toString());
                book.setAuthors(autheursBook.getText().toString());
                book.setGenres(genresBook.getText().toString());
                book.setPublisher(publisherBook.getText().toString());
                book.setYear(yearBook.getText().toString());
                book.setId(id);

                bookDb.updateBook(book);
                Toast.makeText(BookActivity.this, "Modifications enregistrées", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}

