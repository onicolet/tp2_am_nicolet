package com.example.uapv1904079.tp2_nicolet;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.database.Cursor;

import static com.example.uapv1904079.tp2_nicolet.BookDbHelper._ID;


public class MainActivity extends AppCompatActivity {

    Cursor cur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BookDbHelper bookDbHelper = new BookDbHelper(this);
        bookDbHelper.populate();

        cur = bookDbHelper.fetchAllBooks();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter (
                this,
                android.R.layout.simple_list_item_2,
                bookDbHelper.fetchAllBooks(),
                new String[]{ BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS },
                new int[] { android.R.id.text1, android.R.id.text2 }
        );

        final ListView listView = (ListView) findViewById(R.id.bookList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener ( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView parent, View view, int position, long id) {

                SQLiteDatabase db = bookDbHelper.getReadableDatabase();
                Cursor cur2 = db.rawQuery(
                        "select * from " +
                            bookDbHelper.TABLE_NAME +
                            " where " + _ID + " = " + Long.toString(id), null
                        );
                cur2.moveToFirst();
                Book selectedBook = bookDbHelper.cursorToBook(cur2);

                Intent intent = new Intent(MainActivity.this, BookActivity.class);

                intent.putExtra("book", selectedBook);
                intent.putExtra("bookId", Long.toString(id));
                startActivity(intent);
            }
        });


        FloatingActionButton add = findViewById(R.id.addBook);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewBookActivity.class);
                startActivity(intent);
            }
        });
    }


}