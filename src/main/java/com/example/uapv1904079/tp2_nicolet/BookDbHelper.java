package com.example.uapv1904079.tp2_nicolet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class BookDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = BookDbHelper.class.getSimpleName();
    public static final String DATABASE_NAME = "book.db";
    public static final String TABLE_NAME = "library";
    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";


    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL (
                "CREATE TABLE " + TABLE_NAME + "(" +
                        _ID + " INTEGER PRIMARY KEY," +
                        COLUMN_BOOK_TITLE + " TEXT," +
                        COLUMN_AUTHORS + " TEXT," +
                        COLUMN_YEAR + " INTEGER," +
                        COLUMN_GENRES + " TEXT," +
                        COLUMN_PUBLISHER + " TEXT" +
                        ")"
        );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        long rowID = 0;

        values.put(COLUMN_BOOK_TITLE, book.getTitle());
        values.put(COLUMN_PUBLISHER, book.getPublisher());
        values.put(COLUMN_AUTHORS, book.getAuthors());
        values.put(COLUMN_YEAR, book.getYear());
        values.put(COLUMN_GENRES, book.getGenres());

        rowID = db.insert(TABLE_NAME, null, values);

        db.close();
        onUpgrade(db, db.getVersion(), db.getVersion() + 1);

        return (rowID != -1);
    }


    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("_id",book.getId());
        content.put("title",book.getTitle());
        content.put("authors",book.getAuthors());
        content.put("year",book.getYear());
        content.put("genres",book.getGenres());
        content.put("publisher",book.getPublisher());

        onUpgrade(db, db.getVersion(), db.getVersion() + 1);
        return db.update(TABLE_NAME, content, _ID + " = ", null);

    }

    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select "+_ID+","+COLUMN_AUTHORS+","+COLUMN_BOOK_TITLE+" from "+TABLE_NAME,null);

        if (cursor != null)
            cursor.moveToFirst();

        return cursor;
    }


    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        cursor.moveToFirst();
        db.delete(
                TABLE_NAME, "_id = ?",
                new String[] {Long.toString(cursor.getLong(cursor.getColumnIndex("_id")))}
        );
        db.close();
    }


    public void populate() {

        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();

        db.close();
    }


    public static Book cursorToBook(Cursor cursor) {
        Book book;
        cursor.moveToFirst();

        book = new Book(
                cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("title")),
                cursor.getString(cursor.getColumnIndex("authors")),
                cursor.getString(cursor.getColumnIndex("year")),
                cursor.getString(cursor.getColumnIndex("genres")),
                cursor.getString(cursor.getColumnIndex("publisher")));

        return book;

    }
}