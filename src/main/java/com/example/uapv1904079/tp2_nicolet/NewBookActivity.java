﻿package com.example.uapv1904079.tp2_nicolet;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class NewBookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        final BookDbHelper bookDb = new BookDbHelper(this);

        //init
        final TextView nameBook = (TextView) findViewById (R.id.nameBook);
        final TextView autheursBook = (TextView) findViewById (R.id.editAuthors);
        final TextView yearBook = (TextView) findViewById (R.id.editYear);
        final TextView genresBook = (TextView) findViewById (R.id.editGenres);
        final TextView publisherBook = (TextView) findViewById (R.id.editPublisher);

        Button saveButton = (Button) findViewById(R.id.button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book book =  new Book(
                        nameBook.getText().toString(),
                        autheursBook.getText().toString(),
                        yearBook.getText().toString(),
                        genresBook.getText().toString(),
                        publisherBook.getText().toString());

                Toast.makeText(NewBookActivity.this, "Nouveau livre enregistré", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
